(ns fp-tutorial
  (:require [clojure.string :as string]))

;;  What is functional programming?

;; Programming with functions! Functions are first-class.

;; First class means you can be... a parameter to a function,
;; returned as a value from a function,
;; and you can be stored in a variable (binding).

;; Functional programming emphasizes programming with /values/.
;; All programming languages have value types. Lets look at some
;; of Clojure's:


;; string

"hello, world"

;; Number

3

;; keyword

:person

;; vector

[3 2 3]

;; hash map

{:first "Tom" :last "Hanks"}

;; set (unordered, no duplicates)

#{1 2 3}

;; function

+


;; All of these simple expressions are value types.
;; When we enter an expression into the REPL,
;; Clojure prints the value of of that expression.

;; Another way to say this is that Clojure
;; e*value*ates the expression.

;; What is a value? A value is something that doesn't change.
;; It's immutable. 1 will always be 1. You can't /change/ the
;; value of 1. Can you imagine what software would be like
;; if you could actually change the quantity that your favorite
;; programming language operated on when you entered 1 in a
;; calculation?


;; There are two more special value types we need to meet
;; before we dive into functions in more detail.

;; symbol

x

;; What happened? We've met a special kind of data type,
;; called a *symbol*. Symbols are a data type that don't
;; evaluate to themselves, they evaluate or refer to something
;; else. We associate a symbol with another value by using
;; a feature of the Clojure language that lets us define
;; a value for a symbol. It's aptly named *def*:


(def x 1)

x

;; So now we've associated x with 1 and typing in x
;; evaluates without error. But what if we want to
;; talk about the symbol x directly, and not the thing
;; referred to by the symbol x? We can do that, too.
;; We just need to tell Clojure not to evaluate x,
;; but just return it:

;; quoting the symbol x

'x

(quote x)


;; Quoting an expression blocks its evaluation.

;; Incidentally, we've already seen the second of the
;; two data types, the list.

;; a list

(1 2 3)

;; Whoops! What happened this time?
;; Well, like symbols, lists have a special evaluation rule.
;; When evaluating a list, Clojure (like all Lisps), expects the
;; first item in the list to be a function, and the rest of the
;; items in the list to be its arguments. Let's have a closer look:


;; function application (aka calling / invoking)

(+ 1 2 3)

;; This may look a bit strange, but if you think about it it's
;; remarkably consistent. *All* functions are invoked by placing
;; them at the head of a list. Clojure makes no distinction between
;; functions you write and functions that are pre-defined.


;; What is a function?

;; A function describes a computational process. It can be given
;; a name (or not), as well as a series of inputs (or not), but it
;; will *always* return a value - even if that value is *nil*,
;; Clojure's value for nothing. This is one important way in
;; which Clojure, and other functional languages differ from
;; imperative languages. Functional languages don't have any
;; statements. In imperative languages, statements are executed
;; for their side-effects rather than their /values/. We'll revisit
;; this topic a little later on.

;; Right now, we now know enough Clojure to define our own functions,
;; so let's try something simple:

(def square
  (fn [x]
    (* x x)))

;; Just as we did previously, we used *def* to associate a
;; symbol, *square*, with a value-a our new function.

;; There's quite a bit going on here, so let's take a closer look.
;; You will have noticed that we've introduced a new /special form/,
;; *fn*. *fn* looks like a function that accepts two parameters: a
;; vector and a list. The first of these is the *argument vector*
;; which contains zero or more *symbols* that will hold the
;; parameter values when the function is called. These symbols can be
;; used by the second "argument" to *fn* - the function body.

;; So, when we invoke the *square* function, we get what we expect.

(square 3)


;; Now in fact, I've been misleading you a bit. *fn* isn't a function at
;; all. It's something called a *special form*. Special forms are, well,
;; special. They don't obey the rules of evaluation we've introduced so
;; far. You may have been wondering why we didn't get an error when
;; we introduced the symbol *x* in the argument list of the function
;; above. The evaluation of *x* is /delayed/ until the *square* function
;; is invoked. And that's one of the primary differences between functions
;; and special forms-the latter can play tricks with evaluation while the
;; former can't. Thankfully, there aren't many special forms in Clojure,
;; and the ones there are a fairly easy to understand.

;; Let's meet another one, called *defn*. *defn* as you might guess
;; combines *def* and *fn* allowing us to create a function and name
;; it in one abbreviated syntax:

(defn square
  [x]
  (* x x))

;; That saves us a couple of characters and a pair of parentheses, but *defn*
;; has a little more up its sleeve:

;; (defn square
;;   "Produces the square of the parameter, x"
;;   [x]
;;   (* x x))


;; That extra bit is a documentation string (docstring for short).
;; You can get the documentation for any function by invoking:

(doc square)

;; what do you think this does?

(source square)

;; You have to admit, that's pretty cool. Now we'll look at one more
;; special form which will makes it possible to write more interesting
;; functions:

(defn abs
  "Returns the absolute value of a number"
  [n]
  (if (<= 0 n) n (* n -1)))


(abs 5)
(abs -5)

;; The *if* special form has the following parts:

(if '<test-expression> '<consequent> '<alternative>)


;; What makes *if* a special form is that only one of the
;; consequent or alternative clauses will be evaluated, depending
;; on the value of the test expression. A *nil* or *false* value
;; will cause the alternative to be evaluated. *Any* other value
;; will cause the consequent to be evaluated.

;; Quick - what's the value of this expression?

(if 0 (* 1 0) (/ 1 0))

;; Now that we've seen a few functions a quite a bit of the language,
;; let's step back for a moment and take stock of some of the points
;; which may not have been so apparent.


;; A Look at Recursion

;; Recursion is when a function calls itself
;; Let's begin by looking at a simple recursive function-factorial.
;; Factorial of 5 is 5 * 4 * 3 * 2 * 1 and is usually represented by
;; the ! (bang) on calculators.

(defn !
  "Calculates the factorial of a number, n"
  [n]
  (if (= n 0)
      1
      (* n (! (- n 1)))))

(! 5) ;; => 120

;; *!* (factorial) is an interesting function so let's dissect it.
;; Here's how the version of factorial we defined above executes

;; (* 5 (! 4))
;; (* 5 (* 4 (! 3)))
;; (* 5 (* 4 (* 3 (! 2))))
;; (* 5 (* 4 (* 3 (* 2 (! 1)))))
;; (* 5 (* 4 (* 3 (* 2 (* 1 (! 0))))))
;; (* 5 (* 4 (* 3 (* 2 (* 1 1)))))
;; (* 5 (* 4 (* 3 (* 2 1))))
;; (* 5 (* 4 (* 3 2)))
;; (* 5 (* 4 6))
;; (* 5 24)
;; 120

;; Look at the shape of that process.
